import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { BoxGeometry, ConeGeometry, SphereGeometry } from 'three';
import gsap from 'gsap';

const generate = document.querySelector('.generate');
const explode = document.querySelector('.explode');
const def = document.querySelector('.default');

const geometries = [new ConeGeometry, new BoxGeometry, new SphereGeometry];
const textureLoader = new THREE.TextureLoader();
const fireTexture = textureLoader.load('/textures/firee.webp');
const waterTexture = textureLoader.load('/textures/water.jpg');
const landTexture = textureLoader.load('/textures/land.jpg');
const textures = [fireTexture, waterTexture, landTexture];
const randomGeo = [];

generate.onclick = function () {
    const widthInput = document.querySelector('.widthInput').value;
    const heightInput = document.querySelector('.heightInput').value;
    const depthInput = document.querySelector('.depthInput').value;
    for (let i = 0; i < widthInput; i++) {
        for (let k = 0; k < heightInput; k++) {
            for (let j = 0; j < depthInput; j++) {
                const mesh = new THREE.Mesh(
                    geometries[Math.floor(Math.random() * geometries.length)],
                    new THREE.MeshBasicMaterial({ map: textures[Math.floor(Math.random() * textures.length)] })
                );
                mesh.position.x = i;
                mesh.position.y = k;
                mesh.position.z = j;
                scene.add(mesh);
                randomGeo.push(mesh);
            }
        }
    }
};

explode.onclick = function () {
    for (let i = 0; i < randomGeo.length; i++) {
        console.log(Math.random())
        gsap.to(randomGeo[i].position, { x: (Math.random() - 0.5) * 20, y: (Math.random() - 0.5) * 20, z: (Math.random() - 0.5) * 20 })
    }
};

const canvas = document.querySelector('canvas.webgl')

const scene = new THREE.Scene()

const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () => {
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})


const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 1;
camera.position.y = 1;
camera.position.z = 20;
scene.add(camera);

const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true;

const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

const tick = () => {
    controls.update()
    renderer.render(scene, camera)
    window.requestAnimationFrame(tick)
}

tick()